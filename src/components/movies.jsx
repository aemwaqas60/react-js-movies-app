import React, {Component} from 'react';
import {getMovies} from "../services/fakeMovieService";
import Like from "./like";

class Movies extends Component {


    state = {
        movies: getMovies()
    }


    handleDeleteMovie = movie => {
        const movies = this.state.movies.filter(m => m !== movie);
        this.setState({movies: movies});

    }

    onLikeHandler = movie => {

        console.log("Liked Handler called");
        const movies = [...this.state.movies];
        const index = movies.indexOf(movie);

        //   movies[index] = {...this.state.movies[index]};
        movies[index].liked = !(movies[index].liked);

        this.setState({movies});
    }


    render() {
        const {length: count} = this.state.movies;
        if (count === 0)
            return <h1>There are no movies in our database</h1>

        return (

            <div>
                <table className="table">
                    <thead>
                    <tr>
                        <th scope="col">Title</th>
                        <th scope="col">Genre</th>
                        <th scope="col">Stock</th>
                        <th scope="col">Rating</th>
                    </tr>
                    </thead>


                    <tbody>
                    {this.state.movies.map(movie => (

                        <tr key={movie._id}>
                            <td>{movie.title}</td>
                            <td>{movie.genre.name}</td>
                            <td>{movie.numberInStock}</td>
                            <td>{movie.dailyRentalRate}</td>
                            <td><Like
                                onLike={() => {
                                    this.onLikeHandler(movie)
                                }}
                                isLiked={movie.liked}
                            />

                            </td>
                            <td>
                                <button className="btn btn-danger"
                                        onClick={() => {
                                            this.handleDeleteMovie(movie)
                                        }}> Delete
                                </button>
                            </td>
                        </tr>
                    ))}
                    </tbody>

                </table>

            </div>


        )
    }


}


export default Movies;
