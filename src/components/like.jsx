import React, {Component} from 'react';


class Like extends Component {

    render() {
        const {onLike, isLiked} = this.props;

        const classes = (isLiked) ? "fa fa-heart" : "fa fa-heart-o";
        return (

            <React.Fragment>
                <i style={{cursor: "pointer"}} onClick={onLike} className={classes} aria-hidden="true"></i>

            </React.Fragment>


        );
    }

}

export default Like;
